-- Active: 1683927646662@@127.0.0.1@3306@sakila
SELECT actor_id, first_name AS Nombre FROM actor
WHERE last_name LIKE '%WILL%'
AND actor_id BETWEEN 100 AND 200
ORDER BY actor_id;

-- uso de LIMIT
SELECT * FROM actor 
ORDER BY actor_id
LIMIT 10;

--selecciona todos los campos NULL
select * FROM address
WHERE address2 is NULL;

--selecciona todos los campos no NULL
select * FROM address
WHERE address2 is NOT NULL;

-- permite buscar campos vacios y nulos
select * from address
WHERE address2 = "" or address2 is NULL;

-- ejercicio
SELECT * FROM address
WHERE district LIKE "Galicia";