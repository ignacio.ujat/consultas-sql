-- Active: 1683927646662@@127.0.0.1@3306@sakila
-- uso de SELECT
select * FROM actor;

SELECT * FROM address;

--select con clausula WHERE
SELECT * FROM actor WHERE last_name = "WILLIS";
SELECT * FROM actor WHERE last_name = "WILLIS" AND actor_id = 83;
SELECT * FROM actor WHERE last_name = "WILLIS" AND actor_id >= 83;

-- uso de la clausula Order BY
SELECT * FROM actor WHERE last_name = "WILLIS" AND actor_id >= 83 ORDER BY actor_id ASC;
SELECT * FROM actor WHERE last_name = "WILLIS" AND actor_id >= 83 ORDER BY actor_id DESC;

-- uso de OR
SELECT * FROM actor
WHERE (last_name = "wILLIS" or last_name = "WILLIAMS")
AND actor_id >= 83
ORDER BY actor_id DESC;

-- uso de IN
SELECT * FROM actor
WHERE last_name IN ("WILLIS", "WILLIAMS")
AND actor_id >= 83
ORDER BY actor_id DESC;

-- Clausula LIKE
SELECT * FROM actor
WHERE last_name LIKE '%WILL%'
AND actor_id >= 83
ORDER BY actor_id DESC;

-- clausula BETWEEN
SELECT * FROM actor
WHERE last_name LIKE '%WILL%'
AND actor_id BETWEEN 100 AND 200
ORDER BY actor_id;
 